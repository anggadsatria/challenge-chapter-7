const passport = require('./passport');

module.exports = {
    restrict_jwt: passport.authenticate('jwt', { session: false }),
    restrict_local_for_whoami: passport.authenticate('local', {
        successRedirect: '/users/whoami'
    }),
    is_authenticated: (req, res, next) => {
        if (req.isAuthenticated()) return next();
    }
};

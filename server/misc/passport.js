const passport = require('passport');
const JwtStrategy = require('passport-jwt').Strategy,
    ExtractJwt = require('passport-jwt').ExtractJwt;
const LocalStrategy = require('passport-local').Strategy;
const opts = {};
const bcrypt = require('bcrypt');
const { User } = require('../models');
opts.jwtFromRequest = ExtractJwt.fromAuthHeaderAsBearerToken();
opts.secretOrKey = process.env.JWT_SECRET;

passport.use(
    new JwtStrategy(opts, async (jwt_payload, done) => {
        User.findOne({
            where: {
                id: jwt_payload.id,
                name: jwt_payload.name,
                email: jwt_payload.email
            }
        })
            .then((user) => done(null, user))
            .catch((err) => done(err, false));
    })
);

passport.use(
    new LocalStrategy({ usernameField: 'email', passwordField: 'password' }, async (email, password, done) => {
        try {
            const foundUser = await User.findOne({
                where: {
                    email: email
                }
            });

            const isPasswordLegit = bcrypt.compareSync(password, foundUser.password);

            if (!foundUser) {
                return done(null, false);
            }
            if (!isPasswordLegit) {
                return done(null, false);
            }
            return done(null, foundUser);
        } catch (err) {
            return done(err);
        }
    })
);

passport.serializeUser((user, done) => done(null, user.id));
passport.deserializeUser((id, done) => {
    User.findByPk(id).then((foundUser) => done(null, foundUser));
});

module.exports = passport;

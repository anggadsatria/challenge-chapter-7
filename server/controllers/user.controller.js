const { User } = require('../models');
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const { OAuth2Client } = require('google-auth-library');
const client = new OAuth2Client('72913000256-l6ov9h7839iosu72pccap7a34an5umg3.apps.googleusercontent.com');

const login = async (req, res) => {
    const { email, password } = req.body;
    const foundUser = await User.findOne({
        where: {
            email: email
        }
    });
    const isValidPassword = bcrypt.compareSync(password, foundUser.password);
    if (isValidPassword) {
        const payload = {
            id: foundUser.id,
            name: foundUser.name,
            email: foundUser.email
        };
        const token = jwt.sign(payload, process.env.JWT_SECRET);
        return res.status(200).json({
            token: token
        });
    }
    return res.status(400).json({
        status: 'Failed',
        message: 'Wrong email or password'
    });
};

const sum = async (req, res) => {
    const { a, b } = req.body;
    req.hasil = +a + +b;
    const hasilhitungDenganFunctionMinus = await minus(req, res);
    return res.send(`${hasilhitungDenganFunctionMinus}`);
};

const minus = async (req, res) => {
    return req.hasil - 10;
};

const form = (req, res) => res.render('contoh');

const register = async (req, res) => {
    const { name, email, password } = req.body;
    const createdUser = await User.create({
        name: name,
        email: email,
        password: password
    });
    return res.status(201).json({
        status: 'User created successfully',
        data: createdUser.id
    });
};
const get = async (req, res) => {
    const allUsers = await User.findAll({
        attributes: {
            exclude: ['createdAt', 'updatedAt', 'password']
        }
    });
    return res.status(200).json({
        data: allUsers
    });
};

const gLogin = (req, res) => {
    const { credential } = req.body;
    async function verify() {
        const ticket = await client.verifyIdToken({
            idToken: credential,
            audience: '72913000256-l6ov9h7839iosu72pccap7a34an5umg3.apps.googleusercontent.com' // Specify the CLIENT_ID of the app that accesses the backend
            // Or, if multiple clients access the backend:
            //[CLIENT_ID_1, CLIENT_ID_2, CLIENT_ID_3]
        });
        const payload = ticket.getPayload();
        return payload;
    }
    verify()
        .then(async (userData) => {
            const createdPassword = bcrypt.hashSync(userData.sub, +process.env.SALT_ROUNDS);
            const foundUser = await User.findOne({
                where: {
                    email: userData.email
                }
            });
            if (!foundUser) {
                const createduser = await User.create({
                    email: userData.email,
                    name: userData.name,
                    password: createdPassword
                });
                const payload = {
                    id: createduser.id,
                    name: createduser.name,
                    email: createduser.email
                };
                const token = jwt.sign(payload, process.env.JWT_SECRET);
                return res.status(200).json({
                    token: token
                });
            }
            // dibawah ini istilahnya seperti 'else' nya
            const isValidPassword = bcrypt.compareSync(userData.sub, foundUser.password);
            if (isValidPassword) {
                const payload = {
                    id: foundUser.id,
                    name: foundUser.name,
                    email: foundUser.email
                };
                const token = jwt.sign(payload, process.env.JWT_SECRET);
                return res.status(200).json({
                    token: token
                });
            }
            return res.status(400).json({
                message: 'Wrong email or password'
            });
        })
        .catch(console.error);
};

const create = async (req, res) => {
    const { name, email, password } = req.body;
    const createdUser = await User.create({
        name: name,
        email: email,
        password: password,
        profile_picture: req.file.path
    });
    return res.status(201).json({
        data: createdUser
    });
};

module.exports = {
    login,
    register,
    get,
    sum,
    minus,
    form,
    gLogin,
    create
};

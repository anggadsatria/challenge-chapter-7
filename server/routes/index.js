const router = require('express').Router();
const userRoutes = require('./user.route.js');

router.get('/', (req, res) => res.render('index'));
router.get('/gauth', (req, res) => res.render('google-login'));
router.get('/debug-sentry', (req, res) => {
    throw new Error('My first Sentry error!');
});
router.use('/users', userRoutes);

module.exports = router;

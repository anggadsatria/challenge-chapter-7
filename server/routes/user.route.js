const router = require('express').Router();
const userController = require('../controllers/user.controller');
const userChecking = require('../misc/restrict');
const upload = require('../misc/multer');

router.get('/whoami', userChecking.restrict_jwt, userController.get);
router.post('/login', userController.login);
router.post('/google-login', userController.gLogin);
router.post('/register', userController.register);
router.post('/all-calculation', userController.sum);
router.get('/form', userController.form);

router.post('/', upload.single('profil_picture'), userController.create);

module.exports = router;

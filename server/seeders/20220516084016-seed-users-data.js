'use strict';

const usersData = require('../masterdata/users.json');
const usersDataMapped = usersData.map((eachUserData) => {
  eachUserData.createdAt = new Date();
  eachUserData.updatedAt = new Date();
  return eachUserData;
});

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.bulkInsert('Users', usersDataMapped, {});
  },

  down: async (queryInterface, Sequelize) => {
    await queryInterface.bulkDelete('Users', null, { truncate: true, restartIdentity: true });
  }
};

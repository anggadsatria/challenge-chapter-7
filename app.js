const express = require('express');
const app = express();

const session = require('express-session');
const morgan = require('morgan')('dev');

const Sentry = require('@sentry/node');
const Tracing = require('@sentry/tracing');

const passport = require('./server/misc/passport');
const routes = require('./server/routes');

app.use(express.urlencoded({ extended: false }));
app.use(express.json());

app.use(
    session({
        secret: process.env.JWT_SECRET,
        resave: false,
        saveUninitialized: false
    })
);

Sentry.init({
    dsn: 'https://9ccbc7bda8514782bdf78b9174b0d418@o1255244.ingest.sentry.io/6423723',
    integrations: [
        // enable HTTP calls tracing
        new Sentry.Integrations.Http({ tracing: true }),
        // enable Express.js middleware tracing
        new Tracing.Integrations.Express({ app })
    ],

    // Set tracesSampleRate to 1.0 to capture 100%
    // of transactions for performance monitoring.
    // We recommend adjusting this value in production
    tracesSampleRate: 1.0
});

// RequestHandler creates a separate execution context using domains, so that every
// transaction/span/breadcrumb is attached to its own Hub instance
app.use(Sentry.Handlers.requestHandler());
// TracingHandler creates a trace for every incoming request
app.use(Sentry.Handlers.tracingHandler());

app.use(passport.initialize());
app.use(passport.session());

app.set('views', './server/views');
app.set('view engine', 'ejs');

app.use(morgan);

app.use(routes);

// The error handler must be before any other error middleware and after all controllers
app.use(Sentry.Handlers.errorHandler());

// Optional fallthrough error handler
app.use(function onError(err, req, res, next) {
    // The error id is attached to `res.sentry` to be returned
    // and optionally displayed to the user for support.
    res.statusCode = 500;
    return res.status(500).json({
        message: err.message
    });
});

module.exports = app;
